package com.springer.editorialtools.pageobjects.Odyssey;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CTSearchPage {

	@FindBy(css = "#profileId")
	private WebElement txtProfileId;
	
	@FindBy(css = "#createProfile")
	private WebElement btnCreateProfile;
	
	@FindBy(css = "#findProfile")
	private WebElement btnFindProfile;
	
	@FindBy(css ="#showSearch")
	private WebElement lnkFieldedSearch;
	
	@FindBy(css =".openProfile")
	private WebElement lnkOpenProfile;
	
	@FindBy(css ="#matching .openProfile")
	private WebElement lnkMatchingProfile;
	
	@FindBy(css ="#related .openProfile")
	private WebElement lnkRelatedProfile;

	@FindBy(css="#deleted .uneditable")
	private WebElement deletedProfiles;
	
	public CTSearchPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
}
