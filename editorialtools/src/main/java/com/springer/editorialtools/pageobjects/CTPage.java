package com.springer.editorialtools.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.springer.editorialtools.utilities._Actions;

import omelet.data.IProperty;

public class CTPage {

	private WebDriver driver;
	
	@FindBy(css=".add.add_icon.editor_visible_only")
	public WebElement btn_Add;
	
	@FindBy(css="#save")
	public WebElement btn_save;
	
	@FindBy(css="#doctype>b")
	public WebElement lbl_TrialProfileId;
	
	@FindBy(css="#cancel")
	public WebElement btn_cancel;
	
	//Reference
	@FindBy(css=".area.adis_standard_area[data-model-name='OfficialTitle']")
	public WebElement studyTitle;
	
	@FindBy(xpath="//th[contains(text(),'Acronym')]")
	public WebElement trialAcronyms;
	
	@FindBy(css=".area.adis_grid_area[data-model-name='TrialName']>table>tbody>tr")
	public List<WebElement> rows_trialAcronyms;
	
	@FindBy(xpath="//th[contains(text(),'Identifier')]")
	public WebElement trialIdentifiers;
	
	@FindBy(xpath="//th[contains(text(),'Code')]")
	public WebElement therapeuticAreas;
	
	
	//Edit References
		//StudyTitle
	@FindBy(xpath="html/body")
	public WebElement txt_studyTitle;
	
	@FindBy(css=".cke_wysiwyg_frame.cke_reset")
	public WebElement iframe_studyTitle;
	
	@FindBy(css="#IsAssumedTitle")
	public WebElement chk_studyTitle_notOfficialTitle;
	
	/*@FindBy(xpath=".//*[@id='save']")
	public WebElement btn_studyTitle_save;*/
	
	@FindBy(xpath=".//*[@id='reference']/div/div[1]")
	public WebElement saveTxtArea;
	
	@FindBy(css="#cke_108")
	public WebElement a_studyTitle_source;
	
	@FindBy(css="#cke_109")
	public WebElement a_studyTitle_find;	
	
	@FindBy(css="#cke_110")
	public WebElement a_studyTitle_replace;
	
	@FindBy(css="#cke_111")
	public WebElement a_studyTitle_specialCharacter;
	
	@FindBy(css="#cke_101_resizer")
	public WebElement studyTitle_resize;
	
	//TrialAcronym
	@FindBy(css="#Acronym")
	public WebElement txt_trialAcronym;
	
	@FindBy(css="#LongName")
	public WebElement txt_trialAcronymLongName;
	
	@FindBy(css=".area.adis_grid_area.activated.selected_area_hidden>table>tbody>tr")
	public List<WebElement> rows_edittrialAcronyms;
	
	//Trial Identifier
	@FindBy(css="#Identifier")
	public WebElement txt_Identifier;
	
	@FindBy(css="#Owner")
	public WebElement txt_Owner;
	
	//Phase 
	@FindBy(xpath = "//span[contains(text(),'Phase')]")
	public WebElement lbl_phase;
		
	@FindBy(xpath = "//Select[@id='Phase']")
	public WebElement ddl_phase;
	
	@FindBy(xpath = "//*[@id='details']/div/div[1]/ul/li/span[2]")
	public WebElement ddl_phaseValue;
	
/*	@FindBy(xpath = "//button[@id='save']")
	public WebElement btn_savePhase;*/
	
	@FindBy(xpath = "//div[@class='ui-pnotify-text']")
	public WebElement message;
	
	
	//study details
	@FindBy(xpath=".//*[@id='details']/div/div[1]/div[1]")
	public WebElement trialComments;	
	
	@FindBy(xpath=".//*[@id='details']/div/div[1]/div[2]")
	public WebElement purpose;	
	
	@FindBy(xpath=".//*[@id='details']/div/div[1]/ul/li/span[2]")
	public WebElement phase;	
	
	@FindBy(xpath=".//*[@id='details']/div/div[2]/ul/li/span[2]")
	public WebElement status;	
	
	@FindBy(xpath=".//*[@id='details']/div/div[2]/div")
	public WebElement substudiesExtensions;	
	
	@FindBy(xpath=".//*[@id='details']/div/div[3]")
	public WebElement design;
	
	@FindBy(xpath=".//*[@id='details']/div/div[4]/ul/li/span[2]")
	public WebElement concept;
	
	@FindBy(xpath=".//*[@id='details']/div/div[5]/ul/li")
	public WebElement location;
	
	//Descriptors
	@FindBy(xpath="#trial_focus_flags")
	public WebElement trialFocus;
	
	@FindBy(xpath=".area.adis_indications_area>h3")
	public WebElement adisIndications;
	
	@FindBy(xpath=".area.adis_drugs_area>h3")
	public WebElement adisDrugs;
	
	@FindBy(xpath=".area.adis_related_rdi_profiles_area>h3")
	public WebElement relatedRDIprofiles;
	
	//Error Window
	@FindBy(css=".ui-dialog-content[style*='display']")
	public WebElement errorMsg_studyTitle;
	
	@FindBy(css=".ui-button[type='button']")
	public WebElement btn_errorOK;
	
	//Study dates
	@FindBy(xpath=".//*[@id='details']/div/div[2]/table")
	public WebElement studyDates;	
	
	@FindBy(css="#PlannedInitiationDate")
	public WebElement txt_plannedInitiationDate;
	
	@FindBy(css="#PlannedEndDate")
	public WebElement txt_plannedEndDate;
	
	@FindBy(css="#PlannedPrimaryCompletionDate")
	public WebElement txt_plannedPrimaryCompletionDate;
	
	@FindBy(css="#ActualInitiationDate")
	public WebElement txt_actualInitiationDate;
	
	@FindBy(css="#ActualPrimaryCompletionDate")
	public WebElement txt_actualPrimaryCompletionDate;
	
	@FindBy(css="#ActualEndDate")
	public WebElement txt_actualEndDate;
	
	//Related RDI Profiles
	@FindBy(css=".area.adis_related_rdi_profiles_area>h3")
	public WebElement relatedRDIProfile;
	
	@FindBy(css="#DrugProfileId")
	public WebElement txt_drugProfileId;
	
	
	public CTPage(WebDriver driver, IProperty prop)
	{
		PageFactory.initElements(driver, this);
	}
	
	public CTPage load(String url)
	{
		driver.get(url);
		return this;
	}
	
	public CTPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	//Common
	public boolean save()
	{
		btn_save.click();
		return true;
	}
	
	//StudyTitle
	public boolean editStudyTitle()
	{
		_Actions.editField(studyTitle);
		_Actions.WaitForElement(txt_studyTitle);
		driver.switchTo().frame(iframe_studyTitle);
		return true;
	}
	
	//Study Identifier
	public boolean addStudyIdentifier(String identifier, String owner)
	{
		_Actions.WaitForElement(btn_Add);
		btn_Add.click();
		_Actions.EnterText(txt_Identifier, identifier);
		_Actions.EnterText(txt_Owner, owner);
		btn_save.click();
		return true;
	}
	
	//Trial Acronym
	public boolean addTrialAcronym(String acronym, String longname)
	{
		_Actions.WaitForElement(btn_Add);
		btn_Add.click();
		_Actions.EnterText(txt_trialAcronym, acronym);
		_Actions.EnterText(txt_trialAcronymLongName, longname);
		return true;
	}

	//Phase
	public boolean updatePhase(String phase)
	{	
		_Actions.editField(lbl_phase);
		Select ddlOption = new Select(ddl_phase);
		ddlOption.selectByVisibleText(phase);
		return true;
	}
	
	//Related RDI profile
	public boolean addRDIProfile(String drugProfileId)
	{
		_Actions.WaitForElement(btn_Add);
		btn_Add.click();
		_Actions.EnterText(txt_drugProfileId, drugProfileId);
		btn_save.click();
		return true;
	}
	
	public String getTrialProfileId()
	{
		String trialIdentifierId = lbl_TrialProfileId.getText();
		return trialIdentifierId.substring(4);
	}
}
