package com.springer.editorialtools.pageobjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.springer.editorialtools.pageobjects.Odyssey.CTSearchPage;
import com.springer.editorialtools.pageobjects.Odyssey.JournalsMeetingsSearchPage;
import com.springer.editorialtools.pageobjects.Odyssey.NewslettersSearchPage;
import com.springer.editorialtools.pageobjects.Odyssey.RDInsightSearchPage;

import omelet.data.IProperty;

public class SideNavigationPage {

	private WebDriver driver;
	
	//Home Page
	@FindBy(css = ".navbarlink[onclick*='AdisCA']")
	private WebElement lnkContentAcquisition2;
	
	@FindBy(css = ".navbarlink[href*='citation']")
	private WebElement lnkCitation;
	
	@FindBy(css =".navbarmenu>a[href*='AdisCTWeb']")
	private WebElement lnkCT;
	
	@FindBy(css = ".navbarlink[href*='dc/dcmain']")
	private WebElement lnkDataCollection;
	
	@FindBy(css = ".navbarlink[href*='journals']")
	private WebElement lnkJournalsMeetings;
	
	@FindBy(css = ".navbarlink[href*='AdisNewslettersWeb']")
	private WebElement lnkNewsletters;
	
	@FindBy(css = ".navbarlink[href*='rndmain']")
	private WebElement lnkRDI;
	
	@FindBy(css = ".navbarlink[href*='thesaurus']")
	private WebElement lnkThesaurus;
	
	@FindBy(css = ".navbarlink[href*='reports']")
	private WebElement lnkReports;
	
	
	
	public SideNavigationPage(WebDriver driver, IProperty prop)
	{
		PageFactory.initElements(driver, this);
	}
	
	public boolean load(String url)
	{
		driver.get(url);
		return true;
	}
	
	public boolean goto_ContentAcquisition()
	{
		driver.switchTo().frame("navbar");
		lnkContentAcquisition2.click();
		driver.switchTo().defaultContent();
		return true;
	}
	
	public CTSearchPage goto_CT()
	{
		driver.switchTo().frame("navbar");
		lnkCT.click();
		driver.switchTo().defaultContent();
		return new CTSearchPage(driver);
	}
	
	public DataCollectionsPage goto_DataCollection()
	{
		driver.switchTo().frame("navbar");
		lnkDataCollection.click();
		return new DataCollectionsPage(driver);
	}
	
	public JournalsMeetingsSearchPage goto_JournalMeetings()
	{
		driver.switchTo().frame("navbar");
		lnkJournalsMeetings.click();
		return new JournalsMeetingsSearchPage(driver);
	}
	
	public NewslettersSearchPage goto_Newsletters()
	{
		driver.switchTo().frame("navbar");
		lnkNewsletters.click();
		return new NewslettersSearchPage(driver);
	}
	
	public RDInsightSearchPage goto_RDInsight()
	{
		driver.switchTo().frame("navbar");
		lnkRDI.click();
		return new RDInsightSearchPage(driver);
	}
	
	
}
