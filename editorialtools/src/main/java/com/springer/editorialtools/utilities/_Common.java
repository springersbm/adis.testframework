package com.springer.editorialtools.utilities;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import omelet.driver.Driver;


public class _Common {
	
	public static int NumberOfRows(List<WebElement> elements)
	{
		int numberOfRows = elements.size() - 2;
		return numberOfRows;
	}
	
	public static boolean UploadXMLToTestNetwork(String filePath, String ftpPath)
	{
		try 
		{
			File srcFile = new File(filePath);
			File destFile = new File(ftpPath);
			FileUtils.copyFile(srcFile, destFile);
			WebDriver driver = Driver.getDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			return true;
		} 
		catch (java.lang.Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public static String GetDate(String format)
	{
		Date dNow = new Date();
	    SimpleDateFormat ft = new SimpleDateFormat(format);
	    return ft.format(dNow);
	}
}
