package com.springer.editorialtools.utilities;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import omelet.driver.Driver;

public class _Actions {

	
	public static void WaitForElement(WebElement element) {
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public static void EnterText(WebElement element, String value)
	{
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 10);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.sendKeys(value);
	}
	
	public static boolean editField(WebElement element)
	{
		Actions action = new Actions(Driver.getDriver());
		WaitForElement(element);
		action.doubleClick(element).perform();
		return true;
	}
}
